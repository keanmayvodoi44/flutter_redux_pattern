import 'package:demo_flutter_redux/src/model/images.dart';

enum LoginStatus{
  not_login, login
}

enum ImageStatus{
  not_load, loading
}

class UpdateStatusUser{
  LoginStatus _status;

  LoginStatus get status => _status;

  UpdateStatusUser(this._status);
}

class UpdateStatusImages{
  ImageStatus _status;

  ImageStatus get status => _status;

  UpdateStatusImages(this._status);
}

class LoadListImages{
  ListImages _list;

  ListImages get list => _list;

  LoadListImages(this._list);
}