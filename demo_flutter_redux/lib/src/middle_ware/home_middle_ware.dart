import 'dart:convert';

import 'package:demo_flutter_redux/src/action/actions.dart';
import 'package:demo_flutter_redux/src/model/images.dart';
import 'package:demo_flutter_redux/utils/utility.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:redux/redux.dart';
import 'package:redux_thunk/redux_thunk.dart';

ThunkAction<ListImages> loadImages(GlobalKey<ScaffoldState> scaffoldState){
  return (Store<ListImages> store) async {
    ImageStatus _status;
    _status = ImageStatus.loading;

    store.dispatch(UpdateStatusImages(_status));

    ListImages list;

    try{
      http.Response response = await http.get(
        BASE_URL + GETIMAGES_URL,
      );

      if(response.statusCode == 200){
        final mapResponse = json.decode(response.body).cast<Map<String, dynamic>>();
        List<Images> listImages = await mapResponse.map<Images>((json){
          return Images.fromJson(json);
        }).toList();
        list = ListImages(
          status: ImageStatus.not_load,
          images: listImages
        );
      }
      else{
        throw Exception("Không thể kết nối đến Server");
      }
    }
    catch(ex){
      showSnackbar(scaffoldState, "Không thể kết nối đến Server");
    }

    store.dispatch(LoadListImages(list));
  };
}