import 'package:demo_flutter_redux/src/action/actions.dart';
import 'package:demo_flutter_redux/src/model/users.dart';
import 'package:demo_flutter_redux/src/view/home.dart';
import 'package:demo_flutter_redux/utils/utility.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:redux/redux.dart';
import 'package:redux_thunk/redux_thunk.dart';

ThunkAction<Users> login(String account, String password, GlobalKey<ScaffoldState> scaffoldState, BuildContext context){
  return (Store<Users> store) async {
    LoginStatus _status;
    _status = LoginStatus.login;

    store.dispatch(UpdateStatusUser(_status));
    try{
      http.Response response = await http.post(
          BASE_URL + LOGIN_URL,
          body: {
            'account': account,
            'password': password
          }
      );

      if(response.statusCode == 200){
        if(response.body == 'failt'){
          showSnackbar(scaffoldState, "Tài khoản không hợp lệ");
        }
        else{
          Navigator.pushReplacement(
              context,
              MaterialPageRoute(
                  builder: (context) => Home()
              )
          );
        }
      }
      else{
        showSnackbar(scaffoldState, "Không thể kết nối đến Server");
      }
    }
    catch(ex){
      showSnackbar(scaffoldState, "Không thể kết nối đến Server");
    }

    _status = LoginStatus.not_login;
    store.dispatch(UpdateStatusUser(_status));
  };
}