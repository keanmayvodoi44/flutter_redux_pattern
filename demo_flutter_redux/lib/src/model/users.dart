import 'package:demo_flutter_redux/src/action/actions.dart';

class Users{
  String account;
  String password;
  LoginStatus status;

  Users({this.account, this.password, this.status});

  factory Users.fromJson(Map<String, dynamic> json){
    return Users(
        account: json['Account'],
        password: json['Password'],
        status: LoginStatus.not_login
    );
  }
}