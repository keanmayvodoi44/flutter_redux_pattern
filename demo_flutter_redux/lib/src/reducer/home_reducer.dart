import 'package:demo_flutter_redux/src/action/actions.dart';
import 'package:demo_flutter_redux/src/model/images.dart';

ListImages reducer(ListImages list, dynamic action){
  if(action is UpdateStatusImages){
    ListImages newList = ListImages(
      status: action.status,
      images: list.images
    );

    return newList;
  }

  else if(action is LoadListImages){
    return action.list;
  }

  return list;
}