import 'package:demo_flutter_redux/src/action/actions.dart';
import 'package:demo_flutter_redux/src/model/users.dart';

Users reducer (Users user, dynamic action){
  if(action is UpdateStatusUser){
    Users newUser = Users(
      account: "",
      password: "",
      status: action.status
    );

    return newUser;
  }

  return user;
}