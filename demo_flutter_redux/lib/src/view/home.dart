import 'package:demo_flutter_redux/src/action/actions.dart';
import 'package:demo_flutter_redux/src/model/images.dart';
import 'package:flutter/material.dart';
import 'package:demo_flutter_redux/utils/utility.dart';
import 'package:redux/redux.dart';
import 'package:demo_flutter_redux/src/reducer/home_reducer.dart';
import 'package:redux_thunk/redux_thunk.dart';
import 'package:demo_flutter_redux/src/middle_ware/home_middle_ware.dart';
import 'package:flutter_redux/flutter_redux.dart';

class Home extends StatefulWidget{
  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return HomeState();
  }
}

class HomeState extends State<Home>{
  final GlobalKey<ScaffoldState> _scaffoldState = new GlobalKey<ScaffoldState>();

  static List<Images> list;

  Store<ListImages> store = Store<ListImages>(
    reducer,
    initialState: ListImages(
      status: ImageStatus.loading,
      images: list
    ),
    middleware: [thunkMiddleware]
  );

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    store.dispatch(loadImages(_scaffoldState));
  }

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return StoreProvider<ListImages>(
      store: store,
      child: Scaffold(
        key: _scaffoldState,
        appBar: _buildAppBar(),
        body: _buildBody(),
      ),
    );
  }

  Widget _buildAppBar(){
    return AppBar(
      title: Text("Trang chính"),
      centerTitle: true,
    );
  }

  Widget _buildBody(){
    return StoreConnector<ListImages, ListImages>(
      converter: (store) => store.state,
      builder: (_, list){
        return list.status == ImageStatus.loading?_buildProgressDialog():_buildGridView(list.images);
      },
    );
  }

  Widget _buildProgressDialog(){
    return Opacity(
      opacity: 0.5,
      child: Container(
        color: Colors.grey,
        height: MediaQuery.of(context).size.height - MediaQuery.of(context).padding.top,
        width: MediaQuery.of(context).size.width,
        child: Center(
          child: CircularProgressIndicator(),
        ),
      ),
    );
  }

  Widget _buildGridView(List<Images> images){
    return Container(
      child: GridView.count(
        crossAxisCount: 2,
        children: _buildGridTile(images),
      ),
    );
  }

  List<Widget> _buildGridTile(List<Images> images){
    List<Container> containers = List<Container>.generate(images.length, (index){
    return Container(
    padding: EdgeInsets.all(5.0),
    child: FadeInImage.assetNetwork(
    image: BASE_URL + images[index].url,
    placeholder: 'assets/loading.gif',
    fit: BoxFit.fill,
    )
    );
    });
    return containers;
  }
}