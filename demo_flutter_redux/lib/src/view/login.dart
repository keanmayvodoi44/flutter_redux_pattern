import 'package:demo_flutter_redux/src/action/actions.dart';
import 'package:demo_flutter_redux/src/model/users.dart';
import 'package:demo_flutter_redux/src/view/home.dart';
import 'package:flutter/material.dart';
import 'package:redux/redux.dart';
import 'package:flutter_redux/flutter_redux.dart';
import 'package:redux_thunk/redux_thunk.dart';

import 'package:demo_flutter_redux/utils/utility.dart';
import 'package:demo_flutter_redux/src/reducer/login_reducer.dart';
import 'package:demo_flutter_redux/src/middle_ware/login_middle_ware.dart';

class Login extends StatefulWidget{
  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return _LoginState();
  }
}

class _LoginState extends State<Login>{
  final FocusNode _accountFocus = FocusNode();
  final FocusNode _passwordFocus = FocusNode();

  TextEditingController _accountController = TextEditingController();
  TextEditingController _passwordController = TextEditingController();

  final GlobalKey<ScaffoldState> _scaffoldState = new GlobalKey<ScaffoldState>();

  Store<Users> store = Store<Users>(
    reducer,
    initialState: Users(
      account: "",
      password: "",
      status: LoginStatus.not_login
    ),
    middleware: [thunkMiddleware]
  );

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return StoreProvider<Users>(
      store: store,
      child: Scaffold(
        key: _scaffoldState,
        appBar: _buildAppBar(),
        body: _buildBody(),
      ),
    );
  }

  Widget _buildAppBar(){
    return AppBar(
      title: Text("Login"),
      centerTitle: true,
    );
  }

  Widget _buildBody() {
    return StoreConnector<Users, LoginStatus>(
      converter: (store) => store.state.status,
      builder: (_, status){
        return Stack(
          children: <Widget>[
            ListView(
              children: <Widget>[
                Container(
                  height: MediaQuery.of(context).size.height - MediaQuery.of(context).padding.top,
                  padding: EdgeInsets.only(left: 15.0, right: 15.0),
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    children: <Widget>[
                      _buildLoginForm(),
                      _buildButton()
                    ],
                  ),
                )
              ],
            ),
            status == LoginStatus.login?_buildProgressDialog():Container()
          ],
        );
      },
    );
  }

  Widget _buildProgressDialog(){
    return Opacity(
      opacity: 0.5,
      child: Container(
        color: Colors.grey,
        height: MediaQuery.of(context).size.height - MediaQuery.of(context).padding.top,
        width: MediaQuery.of(context).size.width,
        child: Center(
          child: CircularProgressIndicator(),
        ),
      ),
    );
  }

  Widget _buildLoginForm(){
    return Column(
      children: <Widget>[
        _buildInputAccount(),
        Divider(),
        _buildInputPassword()
      ],
    );
  }

  Widget _buildInputAccount(){
    return TextFormField(
      controller: _accountController,
      focusNode: _accountFocus,
      decoration: InputDecoration(
          border: OutlineInputBorder(
              borderRadius: BorderRadius.all(Radius.circular(5.0)),
              borderSide: BorderSide(
                  color: Colors.blue,
                  width: 1.0
              )
          ),
          prefixIcon: Icon(
            Icons.person,
            color: Colors.blue,
          ),
          labelText: "Tài khoản",
          labelStyle: TextStyle(
              color: Colors.blue
          )
      ),
      textInputAction: TextInputAction.next,
      keyboardType: TextInputType.emailAddress,
      onFieldSubmitted: (data){
        fieldFocusChange(context, _accountFocus, _passwordFocus);
      },
    );
  }

  Widget _buildInputPassword(){
    return TextFormField(
      controller: _passwordController,
      focusNode: _passwordFocus,
      decoration: InputDecoration(
          border: OutlineInputBorder(
              borderRadius: BorderRadius.all(Radius.circular(5.0)),
              borderSide: BorderSide(
                  color: Colors.blue,
                  width: 1.0
              )
          ),
          prefixIcon: Icon(
            Icons.vpn_key,
            color: Colors.blue,
          ),
          labelText: "Mật khẩu",
          labelStyle: TextStyle(
              color: Colors.blue
          )
      ),
      textInputAction: TextInputAction.done,
      keyboardType: TextInputType.emailAddress,
      obscureText: true,
    );
  }

  Widget _buildButton(){
    return StoreConnector<Users, LoginCallback>(
      converter: (store) => () => store.dispatch(login(_accountController.text, _passwordController.text, _scaffoldState, context)),

      builder: (_, callback){
        return Container(
          decoration: BoxDecoration(
              borderRadius: BorderRadius.all(Radius.circular(5.0)),
              color: Colors.blue
          ),
          child: Row(
            children: <Widget>[
              Expanded(
                child: RaisedButton(
                  elevation: 0.0,
                  child: Text(
                    "Đăng nhập",
                    style: TextStyle(
                        color: Colors.white
                    ),
                  ),
                  color: Colors.blue,
                  onPressed: (){
                    if(_accountController.text == "" || _passwordController.text == ""){
                      showSnackbar(_scaffoldState, "Cần nhập đầy đủ thông tin");
                    }
                    else{
                      callback();
                    }
                  },
                ),
              )
            ],
          ),
        );
      },
    );
  }
}

typedef void LoginCallback();